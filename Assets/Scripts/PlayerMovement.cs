using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    float moveSpeed = 5f;

    void Update()
    {
        float moveInput = Input.GetAxisRaw("Horizontal");
        //rb.velocity = new Vector3(moveInput * moveSpeed, rb.velocity.y, rb.velocity.z);
        transform.position = new Vector2(transform.position.x + moveSpeed * moveInput * Time.deltaTime, transform.position.y);
    }

}
